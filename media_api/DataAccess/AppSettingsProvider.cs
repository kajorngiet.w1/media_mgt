﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace media_api.DataAccess
{
    public static class AppSettingsProvider
    {
        public static string AccessKey { get; set; }
    }
}
