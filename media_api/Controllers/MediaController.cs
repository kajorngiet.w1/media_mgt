﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using media_model;
using media_model.DataTable;
using media_model.DataTables;
using media_model.Media;
using Microsoft.AspNetCore.Mvc;
using media_api.DataAccess;
using System.Linq.Dynamic.Core;
using media_model.Master;
using media_util;

namespace media_api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class MediaController : Controller
    {
        #region //=== Properties ===//

        private readonly ApplicationDbContext _context;
        public MediaController(ApplicationDbContext context)
        {
            _context = context;
        }

        public static BlobStorageService _blob = null;
        public static BlobStorageService blob
        {
            get { return _blob ?? (_blob = new BlobStorageService(AppSettingsProvider.AccessKey)); }
        }

        #endregion

        [HttpGet]
        public ResponseInfo<MediaSearch> GetMediaDropdown()
        {
            ResponseInfo<MediaSearch> response = new ResponseInfo<MediaSearch>();
            MediaSearch search = new MediaSearch();
            search.mediaTypes = GetMediaType();
            search.months = GetMonths();
            search.years = GetYears();
            response.data = search;
            return response;
        }

        [HttpPost]
        public ResponseInfo<DataTableResponse<List<MediaData>>> Search(DataTablesRequest request)
        {
            ResponseInfo<DataTableResponse<List<MediaData>>> responseInfo = new ResponseInfo<DataTableResponse<List<MediaData>>>();

            var recordsTotal = _context.Media.Count();

            var mediaQuery = _context.Media.AsQueryable();

            int Month = (request.Month == null || request.Month == "") ? 0 : Convert.ToInt32(request.Month);
            int Year = (request.Year == null || request.Year == "") ? 0 : Convert.ToInt32(request.Year);

            mediaQuery = mediaQuery.Where(s =>
                    s.FileName.Contains(((request.FileName == null) ? s.FileName : request.FileName)) &&
                    s.FileType == ((request.FileType == null) ? s.FileType : request.FileType) &&
                    s.CreatedOn.Date.Month == ((Month == 0) ? s.CreatedOn.Date.Month : Month) &&
                    s.CreatedOn.Date.Year == ((Year == 0) ? s.CreatedOn.Date.Year : Year))
                    .Select(x => new MediaData
                    {
                        Id = x.Id,
                        ThumbnailUrl = x.ThumbnailUrl,
                        FileName = x.FileName,
                        FileSize = x.FileSize,
                        FileType = x.FileType,
                        CreatedBy = x.CreatedBy,
                        CreatedOnDisplay = x.CreatedOn.ToString("dd/MM/yyyy HH:mm")
                    });

            var recordsFiltered = mediaQuery.Count();

            var sortColumnName = request.Columns.ElementAt(request.Order.ElementAt(0).Column).Name;
            var sortDirection = request.Order.ElementAt(0).Dir.ToLower();

            // using System.Linq.Dynamic.Core
            mediaQuery = mediaQuery.OrderBy($"{sortColumnName} {sortDirection}");

            var skip = request.Start;
            var take = request.Length;
            var data = mediaQuery
                .Skip(skip)
                .Take(take)
                .ToList();
            DataTableResponse<List<MediaData>> response = new DataTableResponse<List<MediaData>>();
            response.Draw = request.Draw;
            response.RecordsTotal = recordsTotal;
            response.RecordsFiltered = recordsFiltered;
            response.Data = data;
            responseInfo.data = response;

            return responseInfo;
        }

        [HttpGet]
        public ResponseInfo<MediaData> GetItem(int Id)
        {
            ResponseInfo<MediaData> response = new ResponseInfo<MediaData>();
            MediaData result = new MediaData();
            var mediaQuery = _context.Media.AsQueryable();
            var media = mediaQuery.Where(s => s.Id == Id).FirstOrDefault();
            result = media;
            response.data = result;
            return response;
        }

        [HttpPost]
        public ResponseInfo<string> Create(MediaData request)
        {
            ResponseInfo<string> response = new ResponseInfo<string>();

            //Check if not exists do process
            var mediaQuery = _context.Media.AsQueryable();
            if (mediaQuery.Where(s => s.FileName == request.FileName).Count() == 0)
            {
                request.FileUrl = blob.UploadFileToBlob(request.FileName, Convert.FromBase64String(request.FileBytes), request.FileType);
                request.ThumbnailUrl = blob.UploadFileToBlob(request.FileName, Convert.FromBase64String(request.Thumbnail), "image/png");

                request.CreatedBy = "user";
                request.CreatedOn = DateTime.Now;
                request.ModifiedOn = DateTime.Now;
                _context.Media.Add(request);
                _context.SaveChanges();
            }
            else
            {
                response.code = "01";
                response.message = "File name is already in use.";
            }

            return response;
        }

        [HttpPost]
        public ResponseInfo<string> Update(MediaData request)
        {
            ResponseInfo<string> response = new ResponseInfo<string>();
            
            //Check if not exists do process
            var mediaQuery = _context.Media.AsQueryable();
            if (mediaQuery.Where(s => s.FileName == request.FileName && s.Id != request.Id).Count() == 0)
            {
                var media = mediaQuery.Where(s => s.Id == request.Id).FirstOrDefault();
                media.FileName = request.FileName;
                media.ModifiedBy = "user";
                media.ModifiedOn = DateTime.Now;

                _context.Media.Update(media);
                _context.SaveChanges();
            }
            else
            {
                response.code = "01";
                response.message = "File name is already in use.";
            }

            return response;
        }

        [HttpPost]
        public ResponseInfo<string> Delete(MediaData request)
        {
            ResponseInfo<string> response = new ResponseInfo<string>();

            var mediaQuery = _context.Media.AsQueryable();
            var media = mediaQuery.Where(s => s.Id == request.Id).FirstOrDefault();
            _context.Media.Remove(media);
            _context.SaveChangesAsync();

            return response;
        }

        protected List<MediaType> GetMediaType()
        {
            List<MediaType> types = new List<MediaType>();
            types.Add(new MediaType() { Code = "", Name = "Please select" });
            var list = _context.MediaType.ToList();
            foreach (var item in list)
            {
                types.Add(item);
            }
            return types;
        }

        protected List<Dropdown> GetMonths()
        {
            List<Dropdown> months = new List<Dropdown>();
            months.Add(new Dropdown() { Code = "", Name = "Please select" });
            var list = _context.Media.GroupBy(g => g.CreatedOn.Month).Select(g => g.Key).ToList();
            foreach (var item in list)
            {
                months.Add(new Dropdown() { Code = item.ToString(), Name = item.ToString() });
            }
            return months;
        }

        protected List<Dropdown> GetYears()
        {
            List<Dropdown> years = new List<Dropdown>();
            years.Add(new Dropdown() { Code = "", Name = "Please select" });
            var list = _context.Media.GroupBy(g => g.CreatedOn.Year).Select(g => g.Key).ToList();
            foreach (var item in list)
            {
                years.Add(new Dropdown() { Code = item.ToString(), Name = item.ToString() });
            }
            return years;
        }
    }
}
