﻿function getValue(targetId) {
    var input = document.getElementById(targetId);
    return input == null ? "" : input.value.trim();
}

function setValue(targetId, val) {
    document.getElementById(targetId).value = val;
}

function Loading(isShow) {
    if (isShow) {
        document.getElementById("vLoading").style.display = 'block';
    }
    else {
        document.getElementById("vLoading").style.display = 'none';
    }
}

function resizeImageToSpecificWidth(input, max, cb) {
    var data;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (event) {
            var img = new Image();
            img.onload = function () {
                if (img.width > max) {
                    var oc = document.createElement('canvas'), octx = oc.getContext('2d');
                    oc.width = img.width;
                    oc.height = img.height;
                    octx.drawImage(img, 0, 0);
                    if (img.width > img.height) {
                        oc.height = (img.height / img.width) * max;
                        oc.width = max;
                    } else {
                        oc.width = (img.width / img.height) * max;
                        oc.height = max;
                    }
                    octx.drawImage(oc, 0, 0, oc.width, oc.height);
                    octx.drawImage(img, 0, 0, oc.width, oc.height);
                    data = oc.toDataURL();
                } else {
                    data = img.src;
                }
                console.log('xxx')
                console.log(data.split(',')[1]);
                cb(data.split(',')[1]);
            };
            img.src = event.target.result;
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function resizeImage(imgSrc, max, cb) {
    var img = new Image();
    img.onload = function () {
        if (img.width > max) {
            var oc = document.createElement('canvas'), octx = oc.getContext('2d');
            oc.width = img.width;
            oc.height = img.height;
            octx.drawImage(img, 0, 0);
            if (img.width > img.height) {
                oc.height = (img.height / img.width) * max;
                oc.width = max;
            } else {
                oc.width = (img.width / img.height) * max;
                oc.height = max;
            }
            octx.drawImage(oc, 0, 0, oc.width, oc.height);
            octx.drawImage(img, 0, 0, oc.width, oc.height);
            data = oc.toDataURL();
        } else {
            data = img.src;
        }
        cb(data.split(',')[1]);
        Loading(false);
    };
    img.src = imgSrc;
}