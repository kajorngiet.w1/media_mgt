﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using media_web.Models;
using media_web.DataAccess;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using media_model.Media;
using media_model;
using media_model.Master;
using media_util;
using media_model.DataTable;
using media_model.DataTables;
using System.Net.Http;
using Newtonsoft.Json;

namespace media_web.Controllers
{
    public class HomeController : Controller
    {
        #region //=== Properties ===//

        private static string baseUrl = AppSettingsProvider.WebApiBaseUrl;
        private static APIService _apiService = null;
        private static APIService apiService
        {
            get { return _apiService ?? (_apiService = new APIService()); }
        }

        #endregion

        public IActionResult Index()
         {
            MediaResponse result = new MediaResponse();
            string responseText = apiService.SendGetRequest(baseUrl + "api/media/GetMediaDropdown", "", null);
            if (!string.IsNullOrEmpty(responseText))
            {
                MediaSearch search = new MediaSearch();
                search = JsonConvert.DeserializeObject<ResponseInfo<MediaSearch>>(responseText).data;
                result.search = search;
            }

            return View("Index", result);
        }
        private int Calculate(int input, int insertVal)
        {
            string strInput = input.ToString();
            int maxNum = 0;
            //List<string> output = new List<string>();

            for (int i = 0; i < strInput.Length + 1; i++)
            {
                int val = Convert.ToInt32(strInput.Insert(i, insertVal.ToString()));
                //output.Add(val.ToString());
                if (maxNum < val)
                {
                    maxNum = val;
                }
            }
            return maxNum;
        }

        [BindProperty]
        public DataTablesRequest DataTablesRequest { get; set; }

        public JsonResult search()
        {
            DataTableResponse<List<MediaData>> response = new DataTableResponse<List<MediaData>>();

            string responseText = apiService.SendRequest(baseUrl + "api/media/search", "application/json", "Post", JsonConvert.SerializeObject(DataTablesRequest), null);
            if (!string.IsNullOrEmpty(responseText))
            {
                var result = JsonConvert.DeserializeObject<ResponseInfo<DataTableResponse<List<MediaData>>>>(responseText);
                response = result.data;
            }
  
            return Json(response);
        }

        public IActionResult Create()
        {
            MediaData result = new MediaData();
            return View("Create", result);
        }

        public IActionResult Edit(int Id)
        {
            if(Id == 0)
            {
                return View("Index");
            }
            else
            {
                MediaData result = new MediaData();
                string responseText = apiService.SendGetRequest(baseUrl + "api/media/GetItem", "?Id=" + Id.ToString(), null);
                if (!string.IsNullOrEmpty(responseText))
                {
                    result = JsonConvert.DeserializeObject<ResponseInfo<MediaData>>(responseText).data;
                }

                return View("Edit", result);
            }            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
