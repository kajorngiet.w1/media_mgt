﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using media_model;
using media_model.Media;
using media_util;
using media_web.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace media_web.Controllers
{
    public class MediaController : Controller
    {
        #region //=== Properties ===//

        private static string baseUrl = AppSettingsProvider.WebApiBaseUrl;
        private static APIService _apiService = null;
        private static APIService apiService
        {
            get { return _apiService ?? (_apiService = new APIService()); }
        }

        #endregion

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult Create(MediaData request)
        {
            ResponseInfo<string> response = new ResponseInfo<string>();

            string responseText = apiService.SendRequest(baseUrl + "api/media/create", "application/json", "Post", JsonConvert.SerializeObject(request), null);
            if (!string.IsNullOrEmpty(responseText))
            {
                response = JsonConvert.DeserializeObject<ResponseInfo<string>>(responseText);
            }

            return Json(response);
        }

        public JsonResult Update(MediaData request)
        {
            ResponseInfo<string> response = new ResponseInfo<string>();

            string responseText = apiService.SendRequest(baseUrl + "api/media/update", "application/json", "Post", JsonConvert.SerializeObject(request), null);
            if (!string.IsNullOrEmpty(responseText))
            {
                response = JsonConvert.DeserializeObject<ResponseInfo<string>>(responseText);
            }

            return Json(response);
        }

        public JsonResult Delete(MediaData request)
        {
            ResponseInfo<string> response = new ResponseInfo<string>();

            string responseText = apiService.SendRequest(baseUrl + "api/media/delete", "application/json", "Post", JsonConvert.SerializeObject(request), null);
            if (!string.IsNullOrEmpty(responseText))
            {
                response = JsonConvert.DeserializeObject<ResponseInfo<string>>(responseText);
            }

            return Json(response);
        }
    }
}
