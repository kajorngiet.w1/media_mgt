﻿using media_model.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace media_model.Media
{
    public class MediaSearch
    {
        [DisplayName("File Name")]
        public string FileName { get; set; }
        [DisplayName("File Type")]
        public string FileType { get; set; }
        [DisplayName("Month (Upload file)")]
        public string Month { get; set; }
        [DisplayName("Year (Upload file)")]
        public string Year { get; set; }
        public List<MediaType> mediaTypes { get; set; }
        public List<Dropdown> months { get; set; }
        public List<Dropdown> years { get; set; }
    }
}
