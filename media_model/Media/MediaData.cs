﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace media_model.Media
{
    public class MediaData
    {
        public Int64 Id { get; set; }
        [DisplayName("File Name")]
        public string FileName { get; set; }
        [DisplayName("File Type")]
        public string FileType { get; set; }
        [DisplayName("File Size (MB)")]
        public decimal FileSize { get; set; }
        [NotMapped]
        [DisplayName("Media File")]
        public string FileBytes { get; set; }
        [NotMapped]
        public string Thumbnail { get; set; }
        public string FileUrl { get; set; }
        [DisplayName("Preview")]
        public string ThumbnailUrl { get; set; }
        [DisplayName("Upload By")]
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        [NotMapped]
        [DisplayName("Upload Date")]
        public string CreatedOnDisplay { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
