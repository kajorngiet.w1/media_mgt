﻿using System;
using System.Collections.Generic;
using System.Text;

namespace media_model.Media
{
    public class MediaResponse
    {
        public MediaSearch search { get; set; }
        public MediaData data { get; set; }
    }
}
