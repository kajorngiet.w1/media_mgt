﻿using System;
using System.Collections.Generic;
using System.Text;

namespace media_model.Media
{
    public class MediaType
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
