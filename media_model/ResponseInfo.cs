﻿using System;
using System.Collections.Generic;
using System.Text;

namespace media_model
{
    public class ResponseInfo<T> where T : class
    {
        public string code { get; set; } = "0";
        public string message { get; set; } = "Success";
        public T data { get; set; }

    }
}
