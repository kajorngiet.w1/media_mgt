﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace media_model.DataTables
{
    public class Order
    {
        public int Column { get; set; }

        public string Dir { get; set; }
    }
}
