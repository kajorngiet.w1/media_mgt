﻿using System;
using System.Collections.Generic;
using System.Text;

namespace media_model.DataTable
{
    public class DataTableResponse<T>where T : class
    {
        public int Draw { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public T Data { get; set; }
    }
}
