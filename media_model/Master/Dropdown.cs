﻿using System;
using System.Collections.Generic;
using System.Text;

namespace media_model.Master
{
    public class Dropdown
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
