﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.IO;

namespace media_util
{
    public class APIService
    {
        public string SendRequest(string Uri, string ContentType, string WebMethod, string BodyRequest, Dictionary<string, string> headers)
        {
            string ResponseText = string.Empty;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(Uri);
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> pair in headers)
                {
                    httpWebRequest.Headers.Add(pair.Key, pair.Value);
                }
            }
            httpWebRequest.Timeout = 60 * 1000 * 15;
            httpWebRequest.ContentType = ContentType;
            httpWebRequest.Method = WebMethod;

            if (BodyRequest != null)
            {
                using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(BodyRequest);
                }
            }

            HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (StreamReader streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                ResponseText = streamReader.ReadToEnd();
            }
            return ResponseText;
        }

        public string SendGetRequest(string Uri, string Param, Dictionary<string, string> headers)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(Uri);
            httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.Timeout = TimeSpan.FromMilliseconds(60 * 1000 * 15);
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> pair in headers)
                {
                    httpClient.DefaultRequestHeaders.Add(pair.Key, pair.Value);
                }
            }
            var response = Task.Run(() => httpClient.GetAsync(Param)).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            httpClient.Dispose();

            return result;
        }
    }
}
